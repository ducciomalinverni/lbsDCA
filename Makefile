# Declaration of variables
CC = g++-6
CFLAGS = -O3 -mtune=native -march=native -std=c++0x -fopenmp 

# File names
EXEC = lbsDCA
SOURCES = $(wildcard src/*.cpp src/alglib-3.8.2/*.cpp)
OBJECTS = $(SOURCES:.cpp=.o)
INCLUDE = -Iinclude/ -Iinclude/alglib-3.8.2

# Main target
default:opt

opt: $(OBJECTS)
	mkdir -p build/opt
	$(CC) ${LDFLAGS} $(OBJECTS) -O3 -fopenmp -o build/opt/$(EXEC) 

debug: $(OBJECTS)
	mkdir -p build/debug
	$(CC) $(OBJECTS) -g -pg -fopenmp -o build/debug/$(EXEC)

# To obtain object files
%.o: %.cpp
	$(CC) $(CFLAGS) ${INCLUDE}  -c   $< -o $@

# To remove generated files
clean:
	rm -f build/debug/$(EXEC) build/opt/$(EXEC) $(OBJECTS)
	clear
tags:
	@cd src; \
	etags *.cpp ../include/*.h; \
	cd ../include; \
	etags ../src/*.cpp *.h; \
	cd ..
