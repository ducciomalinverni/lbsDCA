/*************************************************************************                                                            
Copyright (c) Duccio Malinverni (EPFL)
>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.
                                                                                                                                       
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
*************************************************************************/

#include "Pseudolikelihood.h"

/* Compute the pseudolikelihood value g_r for fixed node r, and the gradient gradr with respect to 
all parameters entering the evaluation of g_r ( q+q*q*N parameters per node)
*/
double g_dg_r(const unsigned int r, const double* wr, double* gradr,const Parameters& parameters){
  
  unsigned int B=parameters.B;
  unsigned int N=parameters.N;
  unsigned int q=parameters.q;
  double* wSeq = parameters.seqWeights;
  double Z=0,logZ=0,g=0;
  double* P_s=new double[q];
  double* expP_s=new double[q];
  std::fill_n(gradr,q+q*q*N,0.);

  MSA msa=parameters.msa;
  unsigned int ms;
  unsigned int msbr;

  for(unsigned int b=0;b<B;b++){
    for(unsigned int s=0;s<q;s++)
       P_s[s]=wr[s];

   for(unsigned int i=0;i<N;i++)
     if(i!=r){
       ms=msa[b*N+i];
       for(unsigned int s=0;s<q;s++)
	 P_s[s]+=wr[J(i,s,ms,q)];       
     }//End if i!=r

   Z=0;

   for(unsigned int s=0;s<q;s++){
     expP_s[s]=std::exp(P_s[s]);
     Z+=expP_s[s];
   }
   logZ=std::log(Z);
   
   msbr=msa[b*N+r];   
   g+= wSeq[b]*(-P_s[msbr]+logZ);
   
   for(unsigned int s=0;s<q;s++){
     P_s[s]=expP_s[s]/Z;
     gradr[s]+=wSeq[b]*P_s[s];
   }
   gradr[msbr]-=wSeq[b]*1;     

    for(unsigned int i=0;i<N;i++)
     if(i!=r){
       ms=msa[b*N+i];
       gradr[J(i,msbr,ms,q)]-= wSeq[b]*1;
       for(unsigned int s=0;s<q;s++)
	 gradr[J(i,s,ms,q)]+= wSeq[b]*P_s[s];
     } //End if i!=r
   } //End loop over B


  for(unsigned int s=0;s<q;s++){
    g+=parameters.lambdaH*wr[s]*wr[s];
    gradr[s]+=2*parameters.lambdaH*wr[s];
    for(unsigned int i=0;i<N;i++)
      if(i!=r)
	for(unsigned int k=0;k<q;k++){
	  gradr[J(i,s,k,q)]+= 2*parameters.lambdaJ*wr[J(i,s,k,q)];
    	  g+=parameters.lambdaJ*wr[J(i,s,k,q)]*wr[J(i,s,k,q)];
	}
  }

  delete[] P_s;
  delete[] expP_s;

  return g;
}
