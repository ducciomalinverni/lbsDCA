# lbsDCA
**lbsDCA** is an open-source C++ implementation of the asymmetric pseudo-likelihood inference method for performing Direct-Coupling Analysis (DCA) on protein families.
The relevant references regarding the method can be found in

    * Ekeberg et al., 2013, Phys Rev E, _Improved contact prediction in proteins: Using pseudolikelihoods to infer Potts models_
    
    * Morcos et al., 2011, PNAS, _Direct-coupling analysis of residue coevolution captures native contacts across many protein families._.

# Compilation and Install 
In order to compile the binaries, you will need a functional C++ compiler supporting OpenMP. Everything should work out of the box on Linux with an up-to-date gcc compiled. On Mac OS X, the lastest CLANG compiler do not feature OpenMP support (yet). To successfully install lbsDCA, grab a working g++ compiler using your favorite package managaer (brew, macports,...).
```shell
$ brew install gcc6
$ port install gcc6
```

To compile the code, simply run make in the root folder of the **lbsDCA** repository. This will create a build/opt/ folder in the current location, where the binary is installed. If you are compiling in Mac OS X, the CC line of the Makefile must be changed to use the gnu C++ compiler installed above.
```shell
# Declaration of variables
CC = g++-6    
CFLAGS = -O3 -mtune=native -march=native -std=c++0x -fopenmp
...
```

# Documentation
Not all options of lbsDCA have been documented here yes. For a complete list of options of lbsDCA, simply call the programm with the -h option
```shell
$ lbsDCA -h
Usage: lbsDCA [options]
       -f    : Fasta file
       -in   : Arbitratry input file
       -o    : Output file [Default: output.dat]
       -lh   : Lambda H [default 0.01]
       -lj   : Lambda J [default 0.01]
       -q    : Number of states q [default 21]
       -t    : Number of threads [default 1]
       -s    : Shuffle the input matrix
       -g    : Consider the gaps coupling when performing Frobenius norm [default no]
       -full : Save complete couplings, i.e. N*N*q*q raw parameters, withoug APC
       -jfull: Save complete couplings in Ising gauge, i.e. N(N-1)*q*q/2 parameters
       -w    : Load weights for sequences from file [default none]
       -wid  : Compute weights for sequences, based on sequence identity [default none]
       -x    : Load initial parameters of the potts model [default none]
       -max  : Maximum number of symbols to keep for each position [all]											 
```
# Basic example
```shell
  $ lbsDCA -f input.fasta -o output
```
The line above performs inference of the Potts model on the sequences in **input.fasta** and saves the DCA scores in the file **output.dat**. Additionally, a file **output_raw.dat** is created.
The standard output file **output.dat** contains the APC corrected DCA scores, whereas the file **output_raw.dat** contains the uncorrected scores.

# Parallel example
```shell
  $ lbsDCA -f input.fasta -t 4 -o output
```
The **-t N** option enables parallelization of the inference using OpenMP. If the number of threads N does not exceed the number of physical cores available, the speedup essentially linear, due to the trivial paralellization of the asymmetric version of PLM.

# Complete example
```shell
  $ lbsDCA -f input.fasta -t 4 -wid 0.9 -full -lh 0.2 -lj 0.3 -o output
```

The **-wid 0.9** option computes sequence weights, by reweighting sequences at a similarity threshold of 90%. The **-full** option saves the complete parameters of the potts model (couplings and fields) in a file **output_fullParams.dat**. The **-lh** and **-lj** options set the regularization parameters for the fields (**lh**) and the couplings (**lj**)

# Format of the output files
The DCA scores output files, both **output.dat** and **output_raw.dat** follow the same format. Because DCA scores are symmetric with irrelevant diagonal, only the upper diagonal is stored. **output.dat** and **output_raw.dat** 
are single column ASCII files, containing the DCA scores in the following order

```shell
S(1,2)
S(1,3)
...
S(1,N-1)
S(1,N)
S(2,3)
...
S(2,N)
...
S(N-1,1)
...
S(N-1,N)
```

The complete parameter files obtained by the **-full** option are stored as binary files and contain all the couplings and fields of the inferred Potts model. Note that these parameters are raw, as obtained after convergence of the gradient descent. They are thus not symmetrized, nor shifted in the Ising gauge.

The **output_fullParams.dat** files are formatted as follows
```shell
h(1,1)
h(1,2)
...
h(1,21)
J(1,1,1,1)
J(1,1,1,2)
...
J(1,1,1,21)
J(1,1,2,1)
...
J(1,1,21,21)
J(1,2,1,1)
...
J(1,N,21,21)
h(2,1)
...
h(2,21)
J(2,1,1,1)
...
J(N,N,21,21)
```

# License 
```
Copyright (c) Duccio Malinverni (EPFL)
>>> SOURCE LICENSE >>>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.                                                                                    

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
```

This code uses the free version of the alglib library (version 3.8.2, GPL 2+), already packaged with this code.