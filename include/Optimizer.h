/*************************************************************************
Copyright (c) Duccio Malinverni (EPFL)
>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
*************************************************************************/

#ifndef DEF_OPTIMIZER
#define DEF_OPTIMIZER

#include "Pseudolikelihood.h"
#include "stdafx.h"
#include "optimization.h"

using namespace alglib;

struct optimParameters{
  unsigned int r;
  Parameters params;
};


void optimizeWr(const unsigned int r, double* __restrict__ w,const  Parameters&  parameters);

#endif
