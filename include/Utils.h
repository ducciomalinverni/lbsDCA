/*************************************************************************
Copyright (c) Duccio Malinverni (EPFL)
>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
*************************************************************************/

#ifndef DEF_UTILS
#define DEF_UTILS

#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <cmath> 
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <numeric>
#include <omp.h>

typedef std::vector<unsigned int> Sequence;
typedef unsigned int* MSA;

struct Parameters{
  unsigned int N;
  unsigned B;
  double Beff;
  unsigned int q;
  MSA msa;
  double lambdaH;
  double lambdaJ;
  unsigned int nThreads;
  bool countGaps;
  bool saveFullCouplings;
  bool saveCouplings;
  double* seqWeights;
  std::string outFile;
  std::string initParamsFile;
};

inline unsigned int J(unsigned int j,unsigned int k, unsigned int l,unsigned int q){
  return q + q*q*j + q*l + k;
}
inline unsigned int Nr(unsigned int r, unsigned int q, unsigned int N){
  return r*(q+q*q*N);
}
inline unsigned int J_up(unsigned int i,unsigned int j, unsigned int N){
  return (i*N - (i+2)*(i+1)/2 +j)*(i<j) + (j*N - (j+2)*(j+1)/2 +i)*(j<i) ;
}

inline double frobeniusNorm(double* w,unsigned int q){
  double norm=0;
  for(unsigned int i=0;i<q*q;i++)
    norm+=w[i]*w[i];
  return std::sqrt(norm);
}
inline double frobeniusNormWithoutGaps(double* w,unsigned int q){
  double norm=0;
  for(unsigned int i=q;i<q*q;i++)
    if(i%q){
      norm+=w[i]*w[i];
    }
  return std::sqrt(norm);
}

void meanJ_i(double* w,unsigned int i, unsigned int j,unsigned int q,unsigned int N,double* meanJi,bool direction);
double meanJ_ij(double* meanJi, unsigned int q);
void shiftCouplingsToIsingGauge(double* w, double* Jising,double*hIsing, unsigned int q, unsigned int N);
void apcTransform(double* S, double* Sapc,unsigned int N);
std::vector<unsigned int> getSortIndexes(std::vector<unsigned int> const& values);
#endif
