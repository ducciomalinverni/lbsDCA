/*************************************************************************
Copyright (c) Duccio Malinverni (EPFL)
>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
*************************************************************************/

#ifndef DEF_INPUTHANDLER
#define DEF_INPUTHANDLER

#include "Utils.h"
#include "FastaHandler.h"

Parameters parseInput(int argc, char** argv);
MSA readASCII(std::string inputFile,unsigned int& N, unsigned int& B);
void shuffleDataMatrix(MSA msa, const unsigned int N, const unsigned int B); 
void loadSeqWeights(double* weights,std::string weightsFile, double& Beff);
void computeSeqIdWeights(double* weights, MSA msa,unsigned int N, unsigned int B, double& Beff, double minId);
void loadInitPottsParams(double* w, std::string paramsFile,unsigned int nParams);
unsigned int  pruneMSA(MSA& msa, const unsigned int N, 
		       const unsigned int B, const unsigned int maxSymbols, unsigned int& q);
#endif
